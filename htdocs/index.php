<?php

// Defines.
define('ROOT_DIR', realpath(__DIR__ . '/..'));
define('WEB_DIR', ROOT_DIR . '/htdocs');
define('APP_DIR', ROOT_DIR . '/app');
define('CONF_DIR', APP_DIR . '/config');
define('STORE_DIR', ROOT_DIR . '/storage');

// Bootstrap the application.
require_once ROOT_DIR . '/vendor/autoload.php';
$app = Nexiyo\Core::init();

// Run.
require_once APP_DIR . '/routes.php';
$app->start();
