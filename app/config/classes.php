<?php

/**
 * Class-related configuration (required by Nexiyo).
 * Details on each section can be found below.
 */

return [
    // Define any PSR-0 autoloading here - key is the namespace, value is the directory or array of directories.
    'psr0' => [

    ],

    // Define any PSR-4 autoloading here - key is the namespace, value is the directory or array of directories.
    'psr4' => [
        'App\\' => [
            APP_DIR . '/controllers',
            APP_DIR . '/models'
        ]
    ],

    // Add any class aliases - key is the alias, value is the original class.
    'aliases' => [
        'DB'        => 'Illuminate\Support\Facades\DB',
        'Eloquent'  => 'Illuminate\Database\Eloquent\Model'
    ]
];
