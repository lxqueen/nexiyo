<?php

/**
 * Database settings go here (required by Nexiyo).
 * https://laravel.com/docs/5.1/database#configuration
 */

return [
    'driver'    => 'mysql',

    'database'  => getenv('DB_NAME'),
    'prefix'    => getenv('DB_PREFIX'),

    'host'      => getenv('DB_HOST'),
    'username'  => getenv('DB_USER'),
    'password'  => getenv('DB_PASS'),
    'port'      => getenv('DB_PORT'),

    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci'
];
