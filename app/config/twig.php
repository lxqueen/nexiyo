<?php

/**
 * Customise the Twig environment (required by Nexiyo).
 * Details on each section can be found below.
 */

return [
    // Twig loader arguments - see the Twig docs for more information.
    'loader' => [
        'debug' => true
    ],

    // Twig extensions - key is the class name, value is the argument/callback passed.
    'extensions' => [
        'Twig_Extension_Debug' => null
    ],

    // Template globals - key is the name, value is well, the value.
    'globals' => [
        '_nx_version'  => '0.7',
        '_base_url'    => getenv('BASE_URL')
    ]
];
