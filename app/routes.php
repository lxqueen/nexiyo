<?php

/**
 * Define all app routes here.
 * http://flightphp.com/learn/
 */

// Index.
Flight::route('/', ['App\\RootController', 'index']);

// Error pages.
Flight::map('notFound', ['App\\RootController', 'error']);
