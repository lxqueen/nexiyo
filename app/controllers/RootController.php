<?php

namespace App;

class RootController extends Controller
{
    public static function index()
    {
        echo \Flight::render('pages/home.twig');
    }

    public static function error()
    {
        if (\Flight::request()->url != '/notfound') return \Flight::redirect('/notfound');

        echo \Flight::render('pages/error.twig');
    }
}
