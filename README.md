# Nexiyo
Nexiyo is my experiment at building a small framework so I could learn more about the individual components. Now it's in a usable state, I thought I'd release it, as a tool for others to learn or even use in small sites.

This is the repository for the project skeleton, for the core functionality behind it visit the **lxqueen/nexiyo-framework** repository instead.

## Features
### Current
 - Full MVC functionality using as few packages as possible
 - Supports Dotenv for environment credentials
 - Built on Flight (router/microframework) - http://flightphp.com/learn/
 - Uses illuminate/database for connections - https://laravel.com/docs/5.1/database#configuration
 - Eloquent models - http://laravel.com/docs/5.1/eloquent
 - Twig templating engine - http://twig.sensiolabs.org/doc/1.x/

### To do (hopefully)
 - Improve error handling in core
 - Migrate Twig to 2.0
 - Provide defaults for dotenv when unset
 - Add DB migration capabilities

## Installing
### Requirements
 - PHP 5.6 or above (7.x series highly recommended)
 - Either Apache with **mod_rewrite** or Nginx
 - MySQL or MariaDB (other databases theoretically supported however I haven't dealt with them)

### Install
 - Clone/download/beam-from-the-mothership this project or run `composer create-project lxqueen/nexiyo`.
 - Copy **.env.example** to **.env** and fill in your details.
 - Run `composer install`.
 - Visit your BASE_URL and see what happens!

## Usage
### Constants
The **index.php** file sets a few constants for easy use across the application. You can change these however it's recommended they stay as they are. The constants are:
 - **ROOT_DIR** - the base directory
 - **WEB_DIR** (ROOT_DIR + /htdocs) - the web root
 - **APP_DIR** (ROOT_DIR + /app) - the app root for custom code
 - **CONF_DIR** (APP_DIR + /config) - folder in app for configuration
 - **STORE_DIR** (ROOT_DIR + /storage) - storage for Twig (maybe other items in the future)

### Configuration
Apart from the Dotenv, you can also modify other items in the **app/config/** directory, or add your own files and grab them with `$app->getConfig(name)`.

#### Classes
The **psr0** and **psr4** arrays can be used to autoload classes using their respective standards. They are loaded via Composer's built-in `ClassLoader`. Alongside this is the **aliases** array, which can be used to (you guessed it) alias classes on load.

#### Twig
The **loader** and **extension** arrays are passed to Twig during initialization for you to add your own configuration or classes. Alongside this is the **globals** array, which is passed through to Twig templates as global values to be used anywhere.

#### Database
This is the array of arguments passed to the Illuminate database capsule. Unless you're doing your own thing with the database or not using MySQL/MariaDB, this can be left alone.

### MVC components
#### Routing
Add all routing code in **app/routes.php**. Read the Flight documentation for more details.

#### Controllers
All controllers should use the namespace **App** (unless you change it in the config, of course) and extend the base controller found in **app/controllers/**.

#### Models
All models should use the namespace **App** (unless you change it in the config, of course) and extend the base model found in **app/models/**. They are based on Eloquent's model, so you can use Laravel's query builder, relationships etc. in your models. Alternatively you can roll your own implementation and not extend the base model.

#### Views
Views are rendered by Twig and found in the **app/views/** folder. To use them just `echo \Flight::render('name.twig', $data)` like you would with vanilla Flight.

### Command line
As there's no migration capabilities currently, you may wish to run some basic creation commands from a terminal. An example file and function is available as **bin/cli.php** and can be run from the project root with `php bin/cli.php`. Basically, this works the same as the web's **index.php** but without calling `$app->start()`, as that launches Flight's routing. This way you have full access to the application. Extend this as you see fit.

**Have fun!**

## License (MIT License)
**Copyright 2017 Alexis Queen.**

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

 - The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
